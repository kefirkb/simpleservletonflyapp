package com.example;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sergey on 12.01.17.
 */
public class DataUtils {

    private static Map<String, String> servletContentIdentities;

    static {
        servletContentIdentities = new HashMap<>();
    }

    synchronized public static void putServletIdentificator(String name, String content) {

        servletContentIdentities.put(name, content);
    }

    public static String getServletContent(String name) {
        return servletContentIdentities.get(name);
    }

}

