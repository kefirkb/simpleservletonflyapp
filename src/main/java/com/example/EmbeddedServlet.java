package com.example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.example.DataUtils.getServletContent;

/**
 * Created by sergey on 12.01.17.
 */
public class EmbeddedServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write("<h1>Hello i am embedded servlet!My name is " + this.getServletName() + "</h1>");
        resp.getWriter().write("<h2>MyContent is " + getServletContent(this.getServletName()) + "</h2>");
    }
}
